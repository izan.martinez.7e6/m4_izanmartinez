<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <xsl:apply-templates select="mundo"/>
    </xsl:template>

    <xsl:template match="mundo">
        <xsl:for-each select="continente">
        <xsl:sort select="nombre" order="ascending"/>
            <h1><xsl:value-of select="nombre"></xsl:value-of></h1>
            <table><xsl:attribute name="style">text-align:left; border: 1px <xsl:value-of select="nombre/@color"/> solid</xsl:attribute>
                    <tr>
                        <th><xsl:attribute name="style">text-align:left; border: 1px <xsl:value-of select="nombre/@color"/> solid</xsl:attribute><h2>Bnadera</h2></th>
                        <th><xsl:attribute name="style">text-align:left; border: 1px <xsl:value-of select="nombre/@color"/> solid</xsl:attribute><h2>Pais</h2></th>
                        <th><xsl:attribute name="style">text-align:left; border: 1px <xsl:value-of select="nombre/@color"/> solid</xsl:attribute><h2>Gobierno</h2></th>
                        <th><xsl:attribute name="style">text-align:left; border: 1px <xsl:value-of select="nombre/@color"/> solid</xsl:attribute><h2>Capital</h2></th>
                    </tr>
                    <xsl:apply-templates select="paises/pais"/>
            </table>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="paises/pais">
                    <tr>
                        <th><xsl:attribute name="style">text-align:left; border: 1px <xsl:value-of select="../../nombre/@color"/> solid</xsl:attribute>
                            <p><img>
                                <xsl:attribute name="src">
                                    img/<xsl:value-of select="foto"/>
                                </xsl:attribute>
                            </img></p>
                        </th>
                        <th><xsl:attribute name="style">text-align:left; border: 1px <xsl:value-of select="../../nombre/@color"/> solid</xsl:attribute><xsl:value-of select="nombre"/></th>
                        <xsl:apply-templates select="nombre"/>
                        <th><xsl:attribute name="style">text-align:left; border: 1px <xsl:value-of select="../../nombre/@color"/> solid</xsl:attribute><xsl:value-of select="capital"/></th>
                    </tr>
    </xsl:template>

    <xsl:template match="nombre">
            <xsl:choose>
                <xsl:when test="@gobierno = 'monarquia'">
                    <th><xsl:attribute name="style">background-color: yellow;text-align:left; border: 1px <xsl:value-of select="../../../nombre/@color"/> solid</xsl:attribute>
                    <xsl:value-of select="@gobierno"></xsl:value-of></th>
                </xsl:when>
                <xsl:when test="@gobierno = 'dictadura'">
                    <th><xsl:attribute name="style">background-color: red;text-align:left; border: 1px <xsl:value-of select="../../../nombre/@color"/> solid</xsl:attribute>
                    <xsl:value-of select="@gobierno"></xsl:value-of></th>
                </xsl:when>
                <xsl:otherwise>
                    <th><xsl:attribute name="style">text-align:left; border: 1px <xsl:value-of select="../../../nombre/@color"/> solid</xsl:attribute><xsl:value-of select="@gobierno"></xsl:value-of></th>
                </xsl:otherwise>
            </xsl:choose>
    </xsl:template>
</xsl:stylesheet>