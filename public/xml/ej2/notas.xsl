<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <html>
            <body>
                <table style="border: 1px black green">
                    <tr style="background-color:lightblue">
                        <th style="text-align:left">Foto</th>
                        <th style="text-align:left">Nombre</th>
                        <th style="text-align:left">Telefono</th>
                        <th style="text-align:left">Apellido</th>
                        <th style="text-align:left">Repetidor</th>
                        <th style="text-align:left">Nota Practica</th>
                        <th style="text-align:left">Nota Examen</th>
                        <th style="text-align:left">Nota Final</th>
                    </tr>
                    <xsl:for-each select="evaluacion/alumno">
                    <xsl:sort select="apellidos" order="descending"/>
                    <tr>
                        <th>
                            <p><img style="height: 20px">
                                <xsl:attribute name="src">
                                    <xsl:value-of select="nombre"/>.png
                                </xsl:attribute>
                            </img></p>
                        </th>
                        <th style="text-align:left; border: 1px black solid"><xsl:value-of select="nombre"></xsl:value-of></th>
                        <th style="text-align:left; border: 1px black solid"><xsl:value-of select="apellidos"></xsl:value-of></th>
                        <th style="text-align:left; border: 1px black solid"><xsl:value-of select="telefono"></xsl:value-of></th>
                        <th style="text-align:left; border: 1px black solid"><xsl:value-of select="@repite"></xsl:value-of></th>
                        <xsl:apply-templates select="notas"/>
                    </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="notas">
        <th style="text-align:left; border: 1px black solid"><xsl:value-of select="practicas"></xsl:value-of></th>
        <th style="text-align:left; border: 1px black solid"><xsl:value-of select="examen"></xsl:value-of></th>
        <xsl:choose>
            <xsl:when test="(practicas + examen) div 2 &gt; 8">
                <th style="color: green; border: 1px black solid; text-align:left"><xsl:value-of select="(practicas + examen) div 2"></xsl:value-of></th>
            </xsl:when>
            <xsl:when test="(practicas + examen) div 2 &lt; 5">
                <th style="color: red; border: 1px black solid; text-align:left"><xsl:value-of select="(practicas + examen) div 2"></xsl:value-of></th>
            </xsl:when>
            <xsl:otherwise>
                <th style="border: 1px black solid; text-align:left"><xsl:value-of select="(practicas + examen) div 2"></xsl:value-of></th>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>