<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <xsl:element name="lista">
            <xsl:for-each select="discos/disco">
                <xsl:variable name="id" select="interpreter/@id"/>
                <xsl:element name="disco">
                    <xsl:value-of select="title[../interpreter[@id=$id]]"/>
                        es interpretado por:
                    <xsl:value-of select="../group[@id = $id]/name"/>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>