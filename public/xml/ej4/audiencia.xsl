<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <xsl:element name="cadena">
            <xsl:element name="nom">
                <xsl:value-of select="programacio/audiencia/cadenes/cadena[@nom = 'Un TV']/@nom"/>
            </xsl:element>
            <xsl:element name="programas">
                <xsl:apply-templates select="programacio/audiencia"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    <xsl:template match="programacio/audiencia">
        <xsl:element name="programa">
            <xsl:attribute name="hora">
                <xsl:value-of select="hora"/>
            </xsl:attribute>
            <xsl:apply-templates select="cadenes"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="cadenes">
        <xsl:element name="nom-programes">
            <xsl:value-of select="cadena[@nom = 'Un TV']"/>
        </xsl:element>
        <xsl:element name="audiencia">
            <xsl:value-of select="cadena[@nom = 'Un TV']/@percentatge"/>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>