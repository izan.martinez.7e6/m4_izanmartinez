# Párrafos y saltos de linea
**Hola buenas**
__Hola buenas__

*Hola buenas*  
***Hola Buenas***
# Encabezado
## Encabezado
### Encabezado
#### Encabezado
##### Encabezado
###### Encabezado
- elemento
- elemento
+ elemento
+ elemento
1. elemento
1. elemento
    1. elemento

~~~cs
Console.WriteLine("Hello World")
~~~
[enlace](https://youtube.com)  
[Enlace al blog][blog]  
[blog]:https://www.blogger.com

![Texto alternativo](ruta)

|Usuario|Correo|Ciudad|
|-------|------|------|
|Juan|juan@gmail.com|Barcelona|