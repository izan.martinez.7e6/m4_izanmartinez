<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">

        <xsl:element name="section">
            <xsl:element name="article">
                <xsl:element name="img">
                    <xsl:attribute name="class">tituloFoto</xsl:attribute>
                    <xsl:attribute name="src">
                        <xsl:value-of select="Videojuegos/Videojuego/InfoGeneral/Foto"/>
                    </xsl:attribute>
                </xsl:element>
            </xsl:element>
        </xsl:element>

        <xsl:element name="section">
            <xsl:attribute name="class">flexJuegos</xsl:attribute>
            <xsl:element name="article">
                <h2>
                    <xsl:value-of select="Videojuegos/Videojuego/Nombre"/>
                </h2>
                <p>
                    <xsl:value-of select="Videojuegos/Videojuego/InfoGeneral/Descripcion"/>
                </p>
            </xsl:element>
        </xsl:element>

    </xsl:template>
</xsl:stylesheet>
